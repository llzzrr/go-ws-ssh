package websocket

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
)

var (
	configPath string
)

func InitConfig() {

	runEnv := os.Getenv("RUN_ENV")
	switch runEnv {
	case "local":
		viper.SetConfigName("/local")
	case "dev":
		viper.SetConfigName("/dev")
	case "test":
		viper.SetConfigName("/test")
	case "gray":
		viper.SetConfigName("/gray")
	case "prod":
		viper.SetConfigName("/prod")
	default:
		viper.SetConfigName("/local")
	}

	if configPath == "" {
		dir, _ := os.Getwd()
		viper.AddConfigPath(dir + "/config") // 添加搜索路径
	} else {
		viper.AddConfigPath(configPath + "/config") // 添加搜索路径
	}

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}
