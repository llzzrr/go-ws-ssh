package websocket

import (
	"bytes"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/prometheus/common/log"
	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh"
	"io"
	"net/http"
	"sync"
	"time"
)

var (
	upGrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024 * 1024 * 10,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
)

//新的Session
type SSHClient struct {
	StdinPipe       io.WriteCloser
	ComboOutput     *safeBuffer //ssh 终端混合输出
	LogBuff         *safeBuffer //保存session的日志
	InputFilterBuff *safeBuffer //用来过滤输入的命令和ssh_filter配置对比的
	Session         *ssh.Session
	WsConn          *websocket.Conn
	WaitGroup       waitGroupWrapper
	Exit            chan bool //退出标识
}
type waitGroupWrapper struct {
	sync.WaitGroup
}

func (w *waitGroupWrapper) Wrap(cb func()) {
	w.Add(1)
	go func() {
		cb()
		w.Done()
	}()
}

//安全缓存结构体
type safeBuffer struct {
	buffer bytes.Buffer
	mu     sync.Mutex
}

//获取字节
func (w *safeBuffer) Bytes() []byte {
	w.mu.Lock()
	defer w.mu.Unlock() //该方法调用结束后，解锁
	return w.buffer.Bytes()
}

//重置
func (w *safeBuffer) Reset() {
	w.mu.Lock()
	defer w.mu.Unlock() //该方法调用结束后，解锁
	w.buffer.Reset()
}

func (w *safeBuffer) Write(p []byte) (n int, err error) {
	w.mu.Lock()
	defer w.mu.Unlock()
	return w.buffer.Write(p)
}

func CreatSSHClient(cols, rows int, client *ssh.Client, wsConn *websocket.Conn) (*SSHClient, error) {
	session, err := client.NewSession()
	if err != nil {
		return nil, err
	}
	//获取读取的结果
	stdinP, err := session.StdinPipe()
	if err != nil {
		return nil, err
	}
	comboWriter := &safeBuffer{}
	logBuf := &safeBuffer{}   //日志缓存
	inputBuf := &safeBuffer{} //输入缓存
	//
	session.Stdout = comboWriter
	session.Stderr = comboWriter

	//创建Xterm模板
	modes := ssh.TerminalModes{
		ssh.ECHO:          0,     // 是否回显
		ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
		ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
	}
	//使用xterm,rows行数cols格式。
	if err := session.RequestPty("xterm", rows, cols, modes); err != nil {
		return nil, err
	}
	//使用远程shell
	err = session.Shell()
	if err != nil {
		return nil, err
	}

	return &SSHClient{
		StdinPipe:       stdinP,
		ComboOutput:     comboWriter,
		LogBuff:         logBuf,
		InputFilterBuff: inputBuf,
		Session:         session,
		WsConn:          wsConn,
		Exit:            make(chan bool),
	}, nil
}

//初始化
func StartWebSocket() {
	wsPort := viper.GetString("ws.port")
	fmt.Println("wsPort:", wsPort)
	//
	http.HandleFunc("/acc", wsConn)
	http.ListenAndServe(":"+wsPort, nil)
}

func (sc *SSHClient) read() {
	wsConn := sc.WsConn //webSocket;
	for {
		select {
		//表示退出
		case <-sc.Exit:
			return
		default:
			_, byteS, err := wsConn.ReadMessage()
			if err != nil {
				log.Error("webSocket读取消息失败", err)
				return
			}
			byteS = []byte(string(byteS)+"\n")
			//暂时不做消息类型处理
			if _, err := sc.StdinPipe.Write(byteS); err != nil {
				log.Error("消息执行失败", err)
				return
			}
			if _, err := sc.LogBuff.Write(byteS); err != nil {
				log.Error("写入日志", err)
			}
		}
	}
}

//写出消息
func (sc *SSHClient) write() {
	wsConn := sc.WsConn //webSocket
	tick := time.NewTicker(time.Millisecond * time.Duration(60))
	defer tick.Stop()
	for {
		select {
		case <-sc.Exit:
			return
		case <-tick.C:
			if sc.ComboOutput == nil {
				return
			}
			byteS := sc.ComboOutput.Bytes()
			if len(byteS) > 0 {
				//向webSocket写数据
				fmt.Println("要写出去的数据：", byteS)
				err := wsConn.WriteMessage(websocket.TextMessage, byteS)
				if err != nil {
					log.Error("ssh sending combo output to webSocket failed")
				}
				//日志缓存
				_, err = sc.LogBuff.Write(byteS)
				if err != nil {
					log.Error("combo output to log buffer failed")
				}
				//缓存清除
				sc.ComboOutput.buffer.Reset()
			}
		}
	}

}

//Close 关闭
func (sc *SSHClient) Close() {
	if sc.Session != nil {
		sc.Session.Close()
	}
	if sc.LogBuff != nil {
		sc.LogBuff = nil
	}
	if sc.ComboOutput != nil {
		sc.ComboOutput = nil
	}
}

//等待
func (sc *SSHClient) Wait() {
	if err := sc.Session.Wait(); err != nil {
		log.Info(err.Error())
		// sws.exit <- true
		close(sc.Exit)
	}
	log.Debug("remote command to exit.")
	close(sc.Exit)
}

//处理成内部调用,处理成链接远程主机
func creatShell() (*ssh.Client, error) {
	//产生连接
	sh, err := ssh.Dial("tcp", viper.GetString("ssh.addr")+":"+viper.GetString("ssh.port"), &ssh.ClientConfig{
		User:            viper.GetString("ssh.user"),
		Auth:            []ssh.AuthMethod{ssh.Password(viper.GetString("ssh.pass"))},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	})
	if err != nil {
		return nil, err
	}
	return sh, nil
}

//websocket连接
func wsConn(w http.ResponseWriter, r *http.Request) {
	//id := r.URL.Query().Get("id")
	//升级为webSocket
	ws, err := upGrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error("upgrade:", err)
		return
	}
	defer ws.Close()
	shell, err := creatShell()
	if err != nil {
		log.Error("creatShell fail:", err)
		return
	}
	defer shell.Close()

	sc, err := CreatSSHClient(80, 80, shell, ws)
	if err != nil {
		log.Error("CreatSSHClient fail", err)
		return
	}

	defer sc.Close()
	//读取数据

	sc.WaitGroup.Wrap(func() { sc.read() })
	sc.WaitGroup.Wrap(func() { sc.write() })
	sc.WaitGroup.Wrap(func() { sc.Wait() })
	sc.Wait()
}
