module gossh

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	github.com/prometheus/common v0.4.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/viper v1.7.1
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0
)
